﻿using NUnit.Framework;
using Warmups.BLL;

namespace Warmups.Tests
{
    [TestFixture]
    public class StringTests
    { 
        [TestCase("Bob", "Hello Bob!")]
        [TestCase("Alice", "Hello Alice!")]
        [TestCase("X", "Hello X!")]
        public void SayHiTest(string name, string expected)
        {
            // arrange
            Strings obj = new Strings();

            // act
            string actual = obj.SayHi(name);

            // assert
            Assert.AreEqual(expected, actual);
        }

        [TestCase("Hi", "Bye", "HiByeByeHi")]
        [TestCase("Yo", "Alice", "YoAliceAliceYo")]
        [TestCase("What", "Up", "WhatUpUpWhat")]
        public void AbbaTest(string a, string b, string expected)
        {
            Strings obj = new Strings();
            
            string actual = obj.Abba(a, b);
            
            Assert.AreEqual(expected, actual);
        }

        [TestCase("i", "Yay", "<i>Yay</i>")]
        [TestCase("i", "Hello", "<i>Hello</i>")]
        [TestCase("cite", "Yay", "<cite>Yay</cite>")]
        public void MakeTagsTest(string a, string b, string expected)
        {
            Strings obj = new Strings();

            string actual = obj.MakeTags(a, b);

            Assert.AreEqual(expected, actual);
        }

        [TestCase("<<>>", "Yay", "<<Yay>>")]
        [TestCase("<<>>", "WooHoo", "<<WooHoo>>")]
        [TestCase("[[]]", "word", "[[word]]")]
        public void InsertWordTests(string a, string b, string expected)
        {
            Strings obj = new Strings();

            string actual = obj.InsertWord(a, b);

            Assert.AreEqual(expected, actual);
        }

        [TestCase("Hello", "lololo")]
        [TestCase("ab", "ababab")]
        [TestCase("Hi", "HiHiHi")]
        public void MultipleEndingsTests(string a, string expected)
        {
            Strings obj = new Strings();

            string actual = obj.MultipleEndings(a);

            Assert.AreEqual(expected, actual);
        }

        [TestCase("WooHoo", "Woo")]
        [TestCase("HelloThere", "Hello")]
        [TestCase("abcdef", "abc")]
        public void FirstHalfTests(string a, string expected)
        {
            Strings obj = new Strings();

            string actual = obj.FirstHalf(a);

            Assert.AreEqual(expected, actual);
        }

        [TestCase("Hello", "ell")]
        [TestCase("java", "av")]
        [TestCase("coding", "odin")]
        public void TrimOneTests(string a, string expected)
        {
            Strings obj = new Strings();

            string actual = obj.TrimOne(a);

            Assert.AreEqual(expected, actual);
        }

        [TestCase("Hello", "hi", "hiHellohi")]
        [TestCase("hi", "Hello", "hiHellohi")]
        [TestCase("aaa", "b", "baaab")]
        public void LongInMiddleTests(string a, string b, string expected)
        {
            Strings obj = new Strings();

            string actual = obj.LongInMiddle(a, b);

            Assert.AreEqual(expected, actual);
        }

        [TestCase("Hello", "lloHe")]
        [TestCase("java", "vaja")]
        [TestCase("Hi", "Hi")]
        public void Rotateleft2(string a, string expected)
        {
            Strings obj = new Strings();

            string actual = obj.Rotateleft2(a);

            Assert.AreEqual(expected, actual);
        }

        [TestCase("Hello", "loHel")]
        [TestCase("java", "vaja")]
        [TestCase("Hi", "Hi")]
        public void Rotateright2(string a, string expected)
        {
            Strings obj = new Strings();

            string actual = obj.RotateRight2(a);

            Assert.AreEqual(expected, actual);
        }

        [TestCase("Hello", true, "H")]
        [TestCase("Hello", false, "o")]
        [TestCase("oh", true, "o")]
        public void TakeOneTests(string a, bool b, string expected)
        {
            Strings obj = new Strings();

            string actual = obj.TakeOne(a, b);

            Assert.AreEqual(expected, actual);
        }

        [TestCase("oddly", true)]
        [TestCase("y", false)]
        [TestCase("oddy", false)]
        public void EndsWithLyTests(string a, bool expected)
        {
            Strings obj = new Strings();
            bool actual = obj.EndsWithLy(a);
            Assert.AreEqual(expected, actual);
        }

        [TestCase("Hello", 2, "Helo")]
        [TestCase("Chocolate", 3, "Choate")]
        [TestCase("Chocolate", 1, "Ce")]
        public void FrontAndBackTests(string a, int b, string expected)
        {
            Strings obj = new Strings();
            string actual = obj.FrontAndBack(a, b);
            Assert.AreEqual(expected, actual);
        }
        
        [TestCase("java", 0, "ja")]
        [TestCase("java", 2, "va")]
        [TestCase("java", 3, "ja")]
        public void TakeTwoFromPositionTests(string a, int b, string expected)
        {
            Strings obj = new Strings();
            string actual = obj.TakeTwoFromPosition(a, b);
            Assert.AreEqual(expected, actual);
        }

        [TestCase("badxx", true)]
        [TestCase("xbadxx", true)]
        [TestCase("xxbadxx", false)]
        public void HasBadTests(string a, bool expected)
        {
            Strings obj = new Strings();
            bool actual = obj.HasBad(a);
            Assert.AreEqual(expected, actual);
        }

        [TestCase("hello", "he")]
        [TestCase("hi", "hi")]
        [TestCase("h", "h@")]
        public void AtFirstTests(string a, string expected)
        {
            Strings obj = new Strings();
            string actual = obj.AtFirst(a);
            Assert.AreEqual(expected, actual);
        }

        [TestCase("last", "chars", "ls")]
        [TestCase("yo", "mama", "ya")]
        [TestCase("hi", "", "h@")]
        public void LastCharsTests(string a, string b, string expected)
        {
            Strings obj = new Strings();
            string actual = obj.LastChars(a, b);
            Assert.AreEqual(expected, actual);
        }

        [TestCase("abc", "cat", "abcat")]
        [TestCase("dog", "cat", "dogcat")]
        [TestCase("abc", "", "abc")]
        public void ConCatTests(string a, string b, string expected)
        {
            Strings obj = new Strings();
            string actual = obj.ConCat(a, b);
            Assert.AreEqual(expected, actual);
        }

        [TestCase("coding", "codign")]
        [TestCase("cat", "cta")]
        [TestCase("ab", "ba")]
        public void SwapLastTests(string a, string expected)
        {
            Strings obj = new Strings();
            string actual = obj.SwapLast(a);
            Assert.AreEqual(expected, actual);
        }

        [TestCase("edited", true)]
        [TestCase("edit", false)]
        [TestCase("ed", true)]
        public void FrontAgainTests(string a, bool expected)
        {
            Strings obj = new Strings();
            bool actual = obj.FrontAgain(a);
            Assert.AreEqual(expected, actual);
        }

        [TestCase("Hello", "Hi", "loHi")]
        [TestCase("Hello", "java", "ellojava")]
        [TestCase("java", "Hello", "javaello")]
        public void MinCatTests(string a, string b, string expected)
        {
            Strings obj = new Strings();
            string actual = obj.MinCat(a, b);
            Assert.AreEqual(expected, actual);
        }

        [TestCase("Hello", "llo")]
        [TestCase("away", "aay")]
        [TestCase("abed", "abed")]
        public void TweakFrontTests(string a, string expected)
        {
            Strings obj = new Strings();
            string actual = obj.TweakFront(a);
            Assert.AreEqual(expected, actual);
        }

        [TestCase("xHix", "Hi")]
        [TestCase("xHi", "Hi")]
        [TestCase("Hxix", "Hxi")]
        public void StripXTests(string a, string expected)
        {
            Strings obj = new Strings();
            string actual = obj.StripX(a);
            Assert.AreEqual(expected, actual);
        }

    }
}
