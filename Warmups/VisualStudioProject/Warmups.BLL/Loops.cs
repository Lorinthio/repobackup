﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Warmups.BLL
{
    public class Loops
    {
        /* Given a string and a non-negative int n, return a 
           larger string that is n copies of the original string. 
        */
        public string StringTimes(string str, int n)
        {
            string result = "";

            for (int i = 1; i <= n; i++)
            {
                result += str;
            }

            return result;
        }

        public string FrontTimes(string str, int n)
        {
            string prefix = str.Substring(0, Math.Min(3, str.Length));
            string newStr = "";
            for (int i=0; i< n; i++)
            {
                newStr += prefix;
            }
            return newStr;
        }

        public int CountXX(string str)
        {
            int xxCount = 0;

            char[] chars = str.ToCharArray();
            for(int i=0; i<chars.Length-1; i++)
            {
                string a = chars[i].ToString();
                string b = chars[i + 1].ToString();
                if(a + b == "xx")
                {
                    xxCount += 1;
                }
            }

            return xxCount;
        }

        public bool DoubleX(string str)
        {
            char[] chars = str.ToCharArray();
            for (int i = 0; i < chars.Length - 1; i++)
            {
                char a = chars[i];
                char b = chars[i + 1];
                if(a == 'x')
                {
                    return b == 'x';
                }
            }
            return false;
        }

        public string EveryOther(string str)
        {
            string word = "";
            char[] chars = str.ToCharArray();
            for (int i = 0; i < chars.Length; i+=2)
            {
                char a = chars[i];
                word += a.ToString();
            }
            return word;
        }

        public string StringSplosion(string str)
        {
            string newString = "";
            for(int i=0; i<=str.Length; i++)
            {
                newString += str.Substring(0, i);
            }
            return newString;
        }

        public int CountLast2(string str)
        {
            string lastTwo = str.Substring(str.Length - 2, 2);
            int lastTwoCount = 0;

            char[] chars = str.ToCharArray();
            for (int i = 0; i < chars.Length - 1; i++)
            {
                string a = chars[i].ToString();
                string b = chars[i + 1].ToString();
                if (a + b == lastTwo)
                {
                    lastTwoCount += 1;
                }
            }

            lastTwoCount -= 1;
            return lastTwoCount;

        }

        public int Count9(int[] numbers)
        {
            int nineCount = 0;
            foreach(var i in numbers)
            {
                if (i == 9)
                {
                    nineCount += 1;
                }
            }
            return nineCount;
        }


        public bool ArrayFront9(int[] numbers)
        {
            for(int i=0; i<Math.Min(4, numbers.Length); i++)
            {
                if(numbers[i] == 9)
                {
                    return true;
                }
            }
            return false;
        }

        public bool Array123(int[] numbers)
        {
            for(int i=0; i<=numbers.Length-3; i++)
            {
                if(numbers[i] == 1 && numbers[i+1] == 2 && numbers[i+2] == 3)
                {
                    return true;
                }
            }
            return false;
        }

        public int SubStringMatch(string a, string b)
        {
            int similarSubs = 0;
            int max = Math.Min(a.Length, b.Length) - 2;

            char[] charsA = a.ToCharArray();
            char[] charsB = b.ToCharArray();
            for (int i = 0; i <= max; i++)
            {
                string a1 = charsA[i].ToString();
                string a2 = charsA[i + 1].ToString();

                string b1 = charsB[i].ToString();
                string b2 = charsB[i + 1].ToString();
                if (a1 + a2 == b1 + b2)
                {
                    similarSubs += 1;
                }
            }

            return similarSubs;
        }

        public string StringX(string str)
        {
            List<char> chars = str.ToCharArray().ToList();
            List<int> toBeRemoved = new List<int>();

            for(int i=1; i<chars.Count-1; i++)
            {
                if(chars[i] == 'x')
                {
                    toBeRemoved.Add(i);
                }
            }

            for(int i=toBeRemoved.Count-1; i>=0; i--)
            {
                chars.RemoveAt(toBeRemoved[i]);
            }

            //Rebuild
            string newString = "";
            foreach(char c in chars)
            {
                newString += c.ToString();
            }
            return newString;
        }

        public string AltPairs(string a)
        {
            List<char> chars = a.ToCharArray().ToList();
            List<char> newChars = new List<char>();
            for(int i=0; i< chars.Count; i += 3)
            {
                newChars.Add(chars[i]);
                //get next letter
                i += 1;
                if(i < chars.Count) { newChars.Add(chars[i]); }
                

            }

            string newString = "";
            foreach(char c in newChars)
            {
                newString += c.ToString();
            }
            return newString;
        }

        public string DoNotYak(string str)
        {
            for(int i=0; i<str.Length-2; i++)
            {
                if(str.Substring(i,1) + str.Substring(i+2,1) == "yk")
                {
                    str = str.Substring(0, i) + str.Substring(i+3, str.Length-(i+3));
                }
            }
            return str;
        }

        public int Array667(int[] numbers)
        {
            int count = 0;
            for(int i=0; i<numbers.Length-1; i++)
            {
                if(numbers[i] == 6 && (numbers[i+1] == 6 || numbers[i+1] == 7))
                {
                    count += 1;
                }
            }
            return count;
        }

        public bool NoTriples(int[] numbers)
        {
            for(int i=0; i<numbers.Length-3; i++)
            {
                if(numbers[i] == numbers[i+1] && numbers[i+1] == numbers[i + 2])
                {
                    return false;
                }
            }
            return true;
        }

        public bool Pattern51(int[] numbers)
        {
            for(int i=0; i<=numbers.Length-3; i++)
            {
                int value = numbers[i];
                if (numbers[i+1] == value+5 && numbers[i+2] == value-1)
                {
                    return true;
                }
            }
            return false;
        }
    }
}
