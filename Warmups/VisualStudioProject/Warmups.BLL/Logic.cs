﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Warmups.BLL
{
    public class Logic
    {
        /* When squirrels get together for a party, they like to have cigars. 
           A squirrel party is successful when the number of cigars is between 
           40 and 60, inclusive. Unless it is the weekend, in which case there is 
           no upper bound on the number of cigars. Return true if the party with 
           the given values is successful, or false otherwise. 
        */
        public bool GreatParty(int cigars, bool isWeekend)
        {
            if (isWeekend)
                return cigars > 40;
            else
                return (cigars >= 40 && cigars <= 60);
        }

        public int CanHazTable(int yourStyle, int dateStyle)
        {
            if ((yourStyle >= 8 && dateStyle > 2) || (yourStyle > 2 && dateStyle >= 8)){
                return 2;
            }
            else {
                if (yourStyle <= 2 || dateStyle <= 2) { return 0; }
                else { return 1; }
            }
        }

        public bool PlayOutside(int temp, bool isSummer)
        {
            int max = 90;
            if (isSummer) { max += 10; }

            return (40 <= temp && temp <= max);
        }

        public int CaughtSpeeding(int speed, bool isBirthday)
        {
            int addMax = 0;
            if (isBirthday) { addMax = 5; }

            if (speed <= (60 + addMax))
            {
                return 0;
            }
            else if (61 <= speed && speed <= 80 + addMax)
            {
                return 1;
            }
            else {
                return 2;
            }
        }

        public int SkipSum(int a, int b)
        {
            int sum = a + b;
            if (10 <= sum && sum <= 19)
            {
                return 20;
            }
            return sum;
        }

        public string AlarmClock(int day, bool vacation)
        {

            if (vacation)
            {
                string alarm = "10:00";
                string alarmWknd = "off";
                if (day == 0 || day == 6)
                {
                    return alarmWknd;
                }
                return alarm;
            }
            else {
                string alarm = "7:00";
                string alarmWknd = "10:00";
                if (day == 0 || day == 6)
                {
                    return alarmWknd;
                }
                return alarm;
            }
        }
        
        public bool LoveSix(int a, int b)
        {
            return (a == 6 || b == 6 || a + b == 6 || Math.Abs(a - b) == 6);
        }

        public bool InRange(int n, bool outsideMode)
        {
            if (!outsideMode)
            {
                return (1 <= n && n <= 10);
            }
            else {
                return (n <= 1 || 10 <= n);
            }
        }

        public bool SpecialEleven(int n)
        {
            int remain = n % 11;
            return (remain == 0 || remain == 1);
        }

        public bool Mod20(int n)
        {
            int remain = n % 20;
            return (remain == 1 || remain == 2);
        }

        public bool Mod35(int n)
        {
            bool remain3 = (n % 3 == 0);
            bool remain5 = (n % 5 == 0);
            if (remain3 || remain5)
            {
                return (!remain3 == remain5);
            }
            return false;
        }

        public bool AnswerCell(bool isMorning, bool isMom, bool isAsleep)
        {
            if (isAsleep)
            {
                return false;
            }
            else if (isMorning)
            {
                return isMom;
            }

            return true;
        }

        public bool TwoIsOne(int a, int b, int c)
        {
            return (a + b == c || a + c == b || b + c == a);
        }

        public bool AreInOrder(int a, int b, int c, bool bOk)
        {
            if (bOk)
            {
                return (a < c && b < c);
            }
            else {
                return (a < b && b < c);
            }
        }

        public bool InOrderEqual(int a, int b, int c, bool equalOk)
        {
            if (equalOk)
            {
                return (a <= b && b <= c);
            }
            else {
                return (a < b && b < c);
            }
        }

        public bool LastDigit(int a, int b, int c)
        {
            int first = a % 10;
            int second = b % 10;
            int third = c % 10;

            return (first == second || second == third || third == first);
        }

        public int RollDice(int die1, int die2, bool noDoubles)
        {
            if (noDoubles)
            {
                if (die1 == die2)
                {
                    die1 += 1;
                    if (die1 > 6)
                    {
                        die1 -= 6;
                    }
                }
            }

            return die1 + die2;
        }
    }
}
