﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Warmups.BLL
{
    public class Arrays
    {
        /* Given an array of ints, return true if 6 appears 
           as either the first or last element in the array. 
           The array will be length 1 or more. 
        */
        public bool FirstLast6(int[] numbers)
        {
            // 0 is always the first index and 
            // Length - 1 of an array is always the last index
            return (numbers[0] == 6 || numbers[numbers.Length - 1] == 6);
        }

        public bool SameFirstLast(int[] numbers)
        {
            if(numbers.Length >= 1)
            {
                return numbers[0] == numbers[numbers.Length - 1];
            }
            return false;
        }

        public int[] MakePi(int n)
        {
            //Return an int array length n containing the first n digits of pi.

            int[] numbers = new int[n];
            string pi = Math.PI.ToString();
            Console.WriteLine(pi);

            //Remove decimal 3.14...
            pi = pi.Remove(1, 1);
            char[] charsInPi = pi.ToCharArray();
            for(int i=0; i< n; i++)
            {
                numbers[i] = int.Parse(charsInPi[i].ToString());
            }

            return numbers;
        }

        public bool commonEnd(int[] a, int[] b)
        {
            return (a[0] == b[0] || a[a.Length - 1] == b[b.Length - 1]);
        }

        public int Sum(int[] numbers)
        {
            int sum = 0;
            foreach(int i in numbers)
            {
                sum += i;
            }
            return sum;
        }

        public int[] RotateLeft(int[] numbers)
        {
            int start = numbers[0];
            for(int i=0; i < numbers.Length - 1; i++)
            {
                numbers[i] = numbers[i + 1];
            }
            numbers[numbers.Length - 1] = start;
            return numbers;
        }

        public int[] Reverse(int[] numbers)
        {
            int[] newNumbers = new int[numbers.Length];

            for(int i=0; i<numbers.Length; i++)
            {
                newNumbers[i] = numbers[numbers.Length - 1 - i];
            }

            return newNumbers;
        }

        public int[] HigherWins(int[] numbers)
        {
            int highValue = Math.Max(numbers[0], numbers[numbers.Length - 1]);
            for(int i=0; i<numbers.Length; i++)
            {
                numbers[i] = highValue;
            }
            return numbers;
        }

        public int[] GetMiddle(int[] a, int[] b)
        {
            int[] newArray = new int[2];
            newArray[0] = a[1];
            newArray[1] = b[1];

            return newArray;
        }

        public bool HasEven(int[] numbers)
        {
            foreach(var i in numbers)
            {
                if(i % 2 == 0)
                {
                    return true;
                }
            }
            return false;
        }

        public int[] KeepLast(int[] numbers)
        {
            int doubleLength = numbers.Length * 2;
            int[] newArray = new int[doubleLength];
            for(int i=0; i<newArray.Length; i++)
            {
                if(i== newArray.Length - 1)
                {
                    newArray[i] = numbers[numbers.Length - 1];
                }
                else
                {
                    newArray[i] = 0;
                }
            }
            return newArray;
        }

        public bool Double23(int[] numbers)
        {
            int twos = 0;
            int threes = 0;

            foreach(var i in numbers)
            {
                if(i == 2)
                {
                    twos += 1;
                }
                else if(i == 3)
                {
                    threes += 1;
                }
            }

            return (twos == 2 || threes == 2);
        }

        public int[] Fix23(int[] numbers)
        {
            if(numbers[0] == 2)
            {
                if(numbers[1] == 3)
                {
                    numbers[1] = 0;
                }
            }

            if(numbers[1] == 2)
            {
                if(numbers[2] == 3)
                {
                    numbers[2] = 0;
                }
            }

            return numbers;
        }

        public bool Unlucky(int[] numbers)
        {
            for(int i=0; i<=1; i++)
            {
                if (numbers[i] == 1)
                {
                    if(numbers[i+1] == 3)
                    {
                        return true;
                    }
                }
            }

            if(numbers[numbers.Length-2] == 1 && numbers[numbers.Length-1] == 3)
            {
                return true;
            }

            return false;
        }

        public int[] make2(int[] a, int[] b)
        {
            int[] array = new int[2];

            int lengthA = a.Length;
            for(int i=0; i< lengthA; i++)
            {
                array[i] = a[i];
            }
            for(int i=0; i< 2- lengthA; i++)
            {
                array[lengthA + i] = b[i];
            }


            return array;
        }
    }
}
