﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Warmups.BLL
{
    public class Conditionals
    {
        /* We have two children, a and b, and the parameters aSmile and 
           bSmile indicate if each is smiling. We are in trouble if they 
           are both smiling or if neither of them is smiling. Return true 
           if we are in trouble. 
        */
        public bool AreWeInTrouble(bool aSmile, bool bSmile)
        {
            return (aSmile == bSmile);
        }

        public bool CanSleepIn(bool isWeekday, bool isVacation)
        {
            return (!isWeekday || isVacation);
        }

        public int SumDouble(int a, int b)
        {
            if (a != b)
            {
                return a + b;
            }
            return 2 * (a + b);
        }

        public int Diff21(int n)
        {
            int diff = Math.Abs(21 - n);
            if (n > 21)
            {
                return 2 * diff;
            }
            return diff;
        }

        public bool ParrotTrouble(bool isTalking, int hour)
        {
            return (isTalking && (20 < hour || hour < 7));
        }

        public bool Makes10(int a, int b)
        {
            return ((a + b == 10) || (a == 10 || b == 10));
        }

        public bool NearHundred(int n)
        {
            return (Math.Abs(100 - n) <= 10 || Math.Abs(200 - n) <= 10);
        }

        public bool PosNeg(int a, int b, bool negative)
        {
            //Given two int values, return true if one is negative and one is positive. 
            //Except if the parameter "negative" is true, then return true only if both are negative. 


            if (negative)
            {
                return (a < 0 && b < 0);
            }


            if (a > b)
            {
                return (a > 0 && b < 0);
            }
            return (a < 0 && b > 0);
        }

        public string NotString(string s)
        {
            if (s.Length < 3 || !(s.Substring(0, 3) == "not"))
            {
                return "not " + s;
            }

            return s;

        }

        public string MissingChar(string str, int n)
        {
            return str.Substring(0, n) + str.Substring(n + 1, str.Length - (n + 1));
        }

        public string FrontBack(string str)
        {
            if (str.Length > 1)
            {
                return str.Substring(str.Length - 1, 1) + str.Substring(1, str.Length - 2) + str.Substring(0, 1);
            }
            else
            {
                return str;
            }
        }

        public string Front3(string str)
        {
            string threeLetters = str.Substring(0, Math.Min(3, str.Length));
            return threeLetters + threeLetters + threeLetters;
        }

        public string BackAround(string str)
        {
            string lastLetter = str.Substring(str.Length - 1, 1);
            return lastLetter + str + lastLetter;
        }

        public bool Multiple3or5(int number)
        {
            return (number % 3 == 0 || number % 5 == 0);
        }

        public bool StartHi(string str)
        {
            return ((str == "hi" || str.StartsWith("hi ")));
        }

        public bool IcyHot(int temp1, int temp2)
        {
            if (temp1 > temp2)
            {
                return (temp1 > 100 && temp2 < 0);
            }
            return (temp2 > 100 && temp1 < 0);
        }

        public bool Between10and20(int a, int b)
        {
            return ((10 <= a && a <= 20) || (10 <= b && b <= 20));
        }

        public bool HasTeen(int a, int b, int c)
        {
            bool aIsTeen = (13 <= a && a <= 19);
            bool bIsTeen = (13 <= b && b <= 19);
            bool cIsTeen = (13 <= c && c <= 19);

            return (aIsTeen || bIsTeen || cIsTeen);
        }

        public bool SoAlone(int a, int b)
        {
            bool aIsTeen = (13 <= a && a <= 19);
            bool bIsTeen = (13 <= b && b <= 19);

            return (aIsTeen != bIsTeen);
        }

        public string RemoveDel(string str)
        {
            bool delAppears = (str.Substring(1, 3) == "del");
            if (delAppears)
            {
                return str.Substring(0, 1) + str.Substring(4, str.Length - 4);
            }
            return str;
        }

        public bool IxStart(string str)
        {
            return (str.Substring(1, 2) == "ix");
        }

        public string StartOz(string str)
        {
            string newString = "";

            if (str.Length > 0)
            {
                if (str.Length > 1)
                {
                    if (str.Substring(0, 1) == "o")
                    {
                        newString += "o";
                    }
                    if (str.Substring(1, 1) == "z")
                    {
                        newString += "z";
                    }
                }
                else {
                    if (str.Substring(0, 1) == "o")
                    {
                        newString = "o";
                    }
                }
            }

            return newString;
        }

        public int Max(int a, int b, int c)
        {
            int max = a;
            if (b > max)
            {
                max = b;
            }
            if (c > max)
            {
                max = c;
            }

            return max;
        }

        public int Closer(int a, int b)
        {
            int diffA = Math.Abs(10 - a);
            int diffB = Math.Abs(10 - b);

            if (diffA == diffB)
            {
                return 0;
            }

            if(diffA < diffB)
            {
                return a;
            }
            else
            {
                return b;
            }
        }

        public bool GotE(string str)
        {
            int preLength = str.Length;
            str = str.Replace("e", "");
            int postLength = str.Length;

            int Es = preLength - postLength;
            return (1 <= Es && Es <= 3);
        }

        public string EndUp(string str)
        {
            if (str.Length < 3) { return str.ToUpper(); }
            else {
                string front = str.Substring(0, str.Length - 3);
                string end = str.Substring(str.Length - 3, 3).ToUpper();

                return front + end;
            }
        }

        public string EveryNth(string str, int n)
        {
            int start = 0;
            StringBuilder newWord = new StringBuilder(0, 256);
            while (start <= str.Length)
            {
                newWord.Append(str.Substring(start, 1));
                start += n;
            }
            return newWord.ToString();
        }
    }
}
