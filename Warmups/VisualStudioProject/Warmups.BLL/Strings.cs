﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Warmups.BLL
{
    public class Strings
    {
        // Given a string name, e.g. "Bob", return a greeting of the form "Hello Bob!". 
        public string SayHi(string name)
        {
            return string.Format("Hello {0}!", name);
        }

        public string Abba(string a, string b)
        {
            return string.Format("{0}{1}{1}{0}", a, b);
        }

        public string MakeTags(string tag, string content)
        {
            return string.Format("<{0}>{1}</{0}>", tag, content);
        }

        public string InsertWord(string container, string word)
        {
            string first = container.Substring(0, 2);
            string last = container.Substring(2, 2);
            return first + word + last;
        }

        public string MultipleEndings(string str)
        {
            string lastTwo = str.Substring(str.Length - 2, 2);
            return lastTwo + lastTwo + lastTwo;
        }

        public string FirstHalf(string str)
        {
            return str.Substring(0, str.Length / 2);
        }

        public string TrimOne(string str)
        {
            return str.Substring(1, str.Length - 2);
        }

        public string LongInMiddle(string a, string b)
        {
            if (a.Length > b.Length)
            {
                return b + a + b;
            }
            else {
                return a + b + a;
            }
        }

        public string Rotateleft2(string str)
        {
            return str.Substring(2, str.Length - 2) + str.Substring(0, 2);
        }

        public string RotateRight2(string str)
        {
            return str.Substring(str.Length - 2, 2) + str.Substring(0, str.Length - 2);
        }

        public string TakeOne(string str, bool fromFront)
        {
            if (fromFront) { return str.Substring(0, 1); }
            else { return str.Substring(str.Length - 1, 1); }
        }

        public string MiddleTwo(string str)
        {
            return str.Substring(str.Length / 2 - 1, 2); //4 2
        }

        public bool EndsWithLy(string str)
        {
            return (str.EndsWith("ly"));
        }

        public string FrontAndBack(string str, int n)
        {
            return str.Substring(0, n) + str.Substring(str.Length - n, n);
        }

        public string TakeTwoFromPosition(string str, int n)
        {
            if (n >= (str.Length - 1) || n <= 0)
            {
                return str.Substring(0, 2);
            }
            else {
                return str.Substring(n, 2);
            }
        }

        public bool HasBad(string str)
        {
            return (str.Substring(0, 3) == "bad" || str.Substring(1, 3) == "bad");
        }

        public string AtFirst(string str)
        {
            if (str.Length >= 2)
            {
                return str.Substring(0, 2);
            }
            else if (str.Length == 1)
            {
                return str.Substring(0, 1) + "@";
            }
            else {
                return "@@";
            }
        }

        public string LastChars(string a, string b)
        {
            if(a == "")
            {
                a = "@";
            }
            if(b == "")
            {
                b = "@";
            }

            return a.Substring(0, 1) + b.Substring(b.Length - 1, 1);
        }

        public string ConCat(string a, string b)
        {
            bool doubleChar = (a.Substring(a.Length - 1, 1) == b.Substring(0, Math.Min(b.Length, 1)));
            if (doubleChar)
            {
                return a + b.Substring(1, b.Length - 1);
            }
            else {
                return a + b;
            }
        }

        public string SwapLast(string str)
        {
            return str.Substring(0, Math.Max(0, str.Length - 2)) + str.Substring(str.Length - 1, 1) + str.Substring(str.Length - 2, 1);
        }

        public bool FrontAgain(string str)
        {
            return (str.Substring(0, 2) == str.Substring(str.Length - 2, 2));
        }

        public string MinCat(string a, string b)
        {
            if (a.Length <= b.Length)
            {
                return a + b.Substring(b.Length - a.Length, a.Length);
            }
            else {
                return a.Substring(a.Length-b.Length, b.Length) + b;
            }
        }

        public string TweakFront(string str)
        {
            //Given a string, return a version without the first 2 chars. Except keep the first char if it is 'a' 
            //and keep the second char if it is 'b'. The string may be any length.

            string letterOne = str.Substring(0, 1);
            string letterTwo = str.Substring(1, 1);
            string total = "";

            if (letterOne == "a")
            {
                total += letterOne;
            }
	        if (letterTwo == "b")
            {
                total += letterTwo;
            }
            total += str.Substring(2, str.Length - 2);
            return total;
        }

        public string StripX(string str)
        {
            if (str.Substring(0, 1) == "x")
            {
                str = str.Substring(1, str.Length - 1);
            }
            if (str.Substring(str.Length - 1, 1) == "x")
            {
                str = str.Substring(0, str.Length - 1);
            }

            return str;
        }
    }
}
