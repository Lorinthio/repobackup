Given a string of even length, return a string made of the middle two chars, so the string "string" yields "ri". The string length will be at least 2. 

MiddleTwo("string") -> "ri"
MiddleTwo("code") -> "od"
MiddleTwo("Practice") -> "ct"

///Test Cases
[TestCase("string", "ri")]
[TestCase("code", "od")]
[TestCase("Practice", "ct")]

public string MiddleTwo(string str ) {

}
