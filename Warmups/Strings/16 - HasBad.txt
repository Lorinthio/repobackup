Given a string, return true if "bad" appears starting at index 0 or 1 in the string, such as with "badxxx" or "xbadxx" but not "xxbadxx". The string may be any length, including 0.

HasBad("badxx") -> true
HasBad("xbadxx") -> true
HasBad("xxbadxx") -> false

///Test Cases
[TestCase("badxx", true)]
[TestCase("xbadxx", true)]
[TestCase("xxbadxx", false)]

public bool HasBad(string str) {

}
