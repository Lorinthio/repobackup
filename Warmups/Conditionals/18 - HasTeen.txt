We'll say that a number is "teen" if it is in the range 13..19 inclusive. Given 3 int values, return true if 1 or more of them are teen. 

HasTeen(13, 20, 10) -> true
HasTeen(20, 19, 10) -> true
HasTeen(20, 10, 12) -> false

///Test Cases
[TestCase(13, 20, 10, true)]
[TestCase(20, 19, 10, true)]
[TestCase(20, 10, 12, false)]

public bool HasTeen(int a, int b, int c) {
	bool aIsTeen = (13 <= a && a <= 19);
	bool bIsTeen = (13 <= b && b <= 19);
	bool cIsTeen = (13 <= c && c <= 19);
	
	return (aIsTeen || bIsTeen || cIsTeen);
}
