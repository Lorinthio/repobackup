Return true if the given non-negative number is a multiple of 3 or a multiple of 5. Use the % "mod" operator

Multiple3or5(3) -> true
Multiple3or5(10) -> true
Multiple3or5(8) -> false

///Test Cases
[TestCase(3, true)]
[TestCase(10, true)]
[TestCase(8, false)]

public bool Multiple3or5(int number) {
	return (number % 3 == 0 || number % 5 == 0);
}
