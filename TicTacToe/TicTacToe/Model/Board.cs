﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TicTacToe.Model
{
    public class Board
    {
        public Dictionary<Vector2, string> _board { get; private set; }

        public Board()
        {
            _board = new Dictionary<Vector2, string>();
            for (int i=0; i<=2; i++)
            {
                for(int j=0; j<=2; j++)
                {
                    _board.Add(new Vector2(i,j), (i + j*3) + "");
                }
            }
        }
    }
}
