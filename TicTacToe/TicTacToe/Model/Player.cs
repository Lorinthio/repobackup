﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TicTacToe.Model
{
    class Player
    {
        public string _name { get; set; }
        public string _mark { get; set; }

        public Player(string name, string mark)
        {
            _name = name;
            _mark = mark;
        }
    }
}
