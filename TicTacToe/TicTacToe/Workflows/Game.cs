﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TicTacToe.Operations;

namespace TicTacToe.Model
{
    class Game
    {
        private Board _board { get; set; }
        private BoardOperations _operations { get; set; }
        Player _Player1 { get; set; }
        Player _Player2 { get; set; }
        int _turn = 0; //turn tracker, 1 = Player1, 2 = Player 2

        public Game()
        {
            Console.Clear();
            getPlayerNames();
            _board = new Board();
            _operations = new BoardOperations();
            while (!play()) { }

        }

        private void getPlayerNames()
        {
            string p1, p2;
            
            Console.WriteLine("Please enter the name of player 1");
            p1 = Console.ReadLine();
            Console.WriteLine("Please enter the name of player 2");
            p2 = Console.ReadLine();

            _Player1 = new Player(p1, "X");
            _Player2 = new Player(p2, "O");
        }

        private bool play()
        {
            _turn += 1; //change player order
            if (_turn >= 3) //if turn = 3, then make it player 1 again
            {
                _turn -= 2;
            }

            clearScreen();
            this._operations.printBoard(_board);
            Player player = this.getPlayerByTurn();
            getPlayerMove(player);

            if (_operations.checkWin(_board))
            {
                clearScreen();
                this._operations.printBoard(_board);
                displayWinner(player);

                return true;
            }
            else if (_operations.isBoardFull(_board))
            {
                clearScreen();
                this._operations.printBoard(_board);
                displayTie();
                return true;
            }
            else
            {
                return false;
            }
        }

        private void displayWinner(Player p)
        {
            string winnerName = p._name;

            Console.WriteLine("");
            Console.WriteLine("-------------------------------------");
            Console.WriteLine("      {0} has won!!!", winnerName);
            Console.WriteLine("-------------------------------------");
            Console.WriteLine("");
        }

        private void displayTie()
        {
            Console.WriteLine("");
            Console.WriteLine("-------------------------------------");
            Console.WriteLine("      Nobody has won!!!");
            Console.WriteLine("   Because it was a tie...");
            Console.WriteLine("-------------------------------------");
            Console.WriteLine("");
        }

        private void clearScreen()
        {
            Console.Clear();
        }

        private Player getPlayerByTurn()
        {
            if (_turn == 1)
            {
                return _Player1;
            }
            else
            {
                return _Player2;
            }
        }

        private void getPlayerMove(Player player)
        {

            Console.Write("{0}, please pick a position to play in... ", player._name);


            int pos = 0;
            if(Int32.TryParse(Console.ReadLine(), out pos))
            {
                Vector2 vecPos = new Vector2(pos);
                if(!_operations.doPlayerMove(player, _board, vecPos))
                {
                    getPlayerMove(player);
                }
                
            }

            
        }
    }
}
