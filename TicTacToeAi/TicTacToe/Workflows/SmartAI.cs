﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TicTacToe.Model;

namespace TicTacToe.Workflows
{
    public class SmartAI : AI
    {
        public SmartAI(Player p)
        {
            _name = "Smart";
            _player = p;
        }

        public override void Play(Board b, Vector2 PlayerMove)
        {
        	
        	//1. Check for a winning move
        	Vector2 winningMove = getLineMove(b, "O");
        	if(winningMove != null){
        		b._board[winningMove] = _player._mark;
        		return;
        	}
        	
        	//2. Block Player
        	Vector2 blockingMove = getLineMove(b, "X");
        	if(blockingMove != null){
        		b._board[blockingMove] = _player._mark;
        		return;
        	}

        	//3. Create a split move
        	
        	//4. Block opponents split
        	
        	//5. Center
        	if(isNumeric(b._board[new Vector2(1,1)])){
        		b._board[new Vector2(1,1)] = _player._mark;
        		return;
        	}
        	
        	//6. Play in opposite corner
        	if(oppositeCorner(b, PlayerMove)){
        		return;
        	}
        	
        	//7. Play in a corner
        	if(getCorner(b)){
        		return;
        	}
        	
        	//8. Play in middle
        	getMiddle(b);
        }
        
        bool oppositeCorner(Board b, Vector2 PlayerMove){
        	//Opposite Corner
        	if((PlayerMove.X == 0 || PlayerMove.X == 2) && (PlayerMove.Y == 0 || PlayerMove.Y == 2)){
        		Vector2 move = new Vector2(PlayerMove.Y,PlayerMove.X); //Opposite of playermove
        		if(isNumeric(b._board[move])){ //If move is valid, then place marker
        			b._board[move] = _player._mark;
        			return true;
        		}
        	}
        	return false;
        }
        
        bool getCorner(Board b){
        	//Empty Corner
        	for(int i = 0; i<=2; i+=2){
        		for(int j = 0; j<=2; j+=2){
        			Vector2 pos = new Vector2(i, j);
        			if(isNumeric(b._board[pos])){
        				b._board[pos] = _player._mark;
        				return true;
        			}
        		}
        	}
        	return false;
        }
        
        void getMiddle(Board b){
        	//Empty Middle Sides
        	Vector2 middle = new Vector2(0,1);
        	if(isNumeric(b._board[middle])){
				b._board[middle] = _player._mark;
				return;
			}
        	middle = new Vector2(1,0);
        	if(isNumeric(b._board[middle])){
				b._board[middle] = _player._mark;
				return;
			}
        	middle = new Vector2(2,1);
        	if(isNumeric(b._board[middle])){
				b._board[middle] = _player._mark;
				return;
			}
        	middle = new Vector2(1,2);
        	if(isNumeric(b._board[middle])){
				b._board[middle] = _player._mark;
				return;
			}
        }
        
        //Get Line Move
        //Based on mark we will either block or win
        //If mark is X, we want to make sure we block that spot
        //If mark is O, we want to make sure we finish the line and win
        private Vector2 getLineMove(Board b, string mark){
        	//local variables
        	int count;
        	List<Vector2> locations = new List<Vector2>();
        	
        	Vector2[] wins = new Vector2[8]; //There are at most 8 winning scenarios
        	//Check Vertical wins
        	for(int i=0; i<=2; i++){
        		count = 0;
        		locations = new List<Vector2>();
        		for(int j=0; j<=2; j++){
        			Vector2 pos = new Vector2(i,j);
        			if(b._board[pos] == mark){ //If the ai placed a mark there
        				count += 1;
        			}
        			else if(isNumeric(b._board[pos])){ //Is an open spot
        				locations.Add(pos);
        			}
        		}
        		if(count == 2 && locations.Count == 1){ //If we had 2 marks in that row and there is an open spot
        			return locations[0];
        		}
        	}
        	
        	//Check Horizontal wins
        	for(int j=0; j<=2; j++){
        		count = 0;
        		locations = new List<Vector2>();
        		for(int i=0; i<=2; i++){
        			Vector2 pos = new Vector2(i,j);
        			if(b._board[pos] ==mark){ //If the mark is here
        				count += 1;
        			}
        			else if(isNumeric(b._board[pos])){ //Is an open spot
        				locations.Add(pos);
        			}
        		}
        		if(count == 2 && locations.Count == 1){ //If we had 2 marks in that row and there is an open spot
        			return locations[0];
        		}
        	}
        	
        	count = 0;
        	locations = new List<Vector2>();
        	//Check TopLeft to BottomRight Diagonal
        	for(int i=0; i<=2; i++){
        		Vector2 pos = new Vector2(i,i);
        		if(b._board[pos] == mark){ //If the mark is there
    				count += 1;
    			}
    			else if(isNumeric(b._board[pos])){ //Is an open spot
    				locations.Add(pos);
    			}
        	}
        	if(count == 2 && locations.Count == 1){ //If we had 2 marks in that row and there is an open spot
    			return locations[0];
    		}
        	
        
        	count = 0;
        	locations = new List<Vector2>();
        	//Check TopRight to BottomLeft Diagonal
        	for(int i=0; i<=2; i++){
        		Vector2 pos = new Vector2(i,2-i);
        		if(b._board[pos] == mark){ //If the ai placed a mark there
    				count += 1;
    			}
    			else if(isNumeric(b._board[pos])){ //Is an open spot
    				locations.Add(pos);
    			}
        	}
        	if(count == 2 && locations.Count == 1){ //If we had 2 marks in that row and there is an open spot
    			return locations[0];
    		}
        	return null;
        }

    }
}
