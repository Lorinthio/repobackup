﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TicTacToe.Model;

namespace TicTacToe.Workflows
{
    public abstract class AI
    {
    	
    	public string _name;
        protected Player _player;
        protected Random random = new Random();
        
        public abstract void Play(Board b, Vector2 move);
        
        public bool isNumeric(string s){
        	return ("0123456789".Contains(s));
        }
    }
}
