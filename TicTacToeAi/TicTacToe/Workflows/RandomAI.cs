﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TicTacToe.Model;
using TicTacToe.Operations;

namespace TicTacToe.Workflows
{
    public class RandomAI : AI
    {
    	

        public RandomAI(Player p)
        {
            _name = "Random";
            _player = p;
        }

        //move isn't checked with random ai
        public override void Play(Board b, Vector2 move)
        {
            int x;
            int y;
            
            bool valid = false;
            do{
            	x = random.Next(0, 3);
            	y = random.Next(0, 3);
            	
            	valid = isNumeric(b._board[new Vector2(x,y)]);            
            }
        	while(!valid);
        	
        	b._board[new Vector2(x,y)] = _player._mark;
            
        }

    }
}
