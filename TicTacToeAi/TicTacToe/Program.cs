﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TicTacToe.Model;

namespace TicTacToe
{
    class Program
    {

        static void Main(string[] args)
        {
            do
            {
                new Game();
            }
            while (playAgain());

            Console.WriteLine("Thanks for playing!");
            Console.ReadKey();
            
        }
        
        static bool playAgain()
        {
            Console.Write("Would you like to play again? (y/n)");
            string response = Console.ReadLine();
            if (response.Contains('y'))
            {
                return true;
            }
            return false;
        }
    }
}
