﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TicTacToe.Model;

namespace TicTacToe.Operations
{
    class BoardOperations
    {
        const string numbers = "0123456789";

        #region "PublicMethods"
        public bool checkWin(Board gameBoard)
        {
            Dictionary<Vector2, string> board = gameBoard._board;
            Vector2 marker = new Vector2(0);
            //Check horizontal wins
            string a, b, c;
            //a b c
            //- - -
            //- - -
            a = board[marker.setPosition2D(0, 0)];
            b = board[marker.setPosition2D(1, 0)];
            c = board[marker.setPosition2D(2, 0)];
            if (checkLine(a, b, c))
            {
                return true;
            }


            //- - -   
            //a b c
            //- - -
            a = board[marker.setPosition2D(0, 1)];
            b = board[marker.setPosition2D(1, 1)];
            c = board[marker.setPosition2D(2, 1)];
            if (checkLine(a, b, c))
            {
                return true;
            }

            //- - -
            //- - -
            //a b c
            a = board[marker.setPosition2D(0, 2)];
            b = board[marker.setPosition2D(1, 2)];
            c = board[marker.setPosition2D(2, 2)];
            if (checkLine(a, b, c))
            {
                return true;
            }

            //Check vertical wins
            //a--
            //b--
            //c--
            a = board[marker.setPosition2D(0, 0)];
            b = board[marker.setPosition2D(0, 1)];
            c = board[marker.setPosition2D(0, 2)];
            if (checkLine(a, b, c))
            {
                return true;
            }

            //-a-
            //-b-
            //-c-
            a = board[marker.setPosition2D(1, 0)];
            b = board[marker.setPosition2D(1, 1)];
            c = board[marker.setPosition2D(1, 2)];
            if (checkLine(a, b, c))
            {
                return true;
            }

            //--a
            //--b
            //--c
            a = board[marker.setPosition2D(2, 0)];
            b = board[marker.setPosition2D(2, 1)];
            c = board[marker.setPosition2D(2, 2)];
            if (checkLine(a, b, c))
            {
                return true;
            }

            //Check diagonal wins
            //a--
            //-b-
            //--c
            a = board[marker.setPosition2D(0, 0)];
            b = board[marker.setPosition2D(1, 1)];
            c = board[marker.setPosition2D(2, 2)];
            if (checkLine(a, b, c))
            {
                return true;
            }

            //--a
            //-b-
            //c--
            a = board[marker.setPosition2D(2, 0)];
            b = board[marker.setPosition2D(1, 1)];
            c = board[marker.setPosition2D(0, 2)];
            if (checkLine(a, b, c))
            {
                return true;
            }

            return false;
        }

        public bool isBoardFull(Board board)
        {
            for(int i=0; i<=2; i++)
            {
                for(int j=0; j<=2; j++)
                {
                    if(isNumber(board._board[new Vector2(i, j)]))
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        public bool doPlayerMove(Player p, Board b, Vector2 pos)
        {
            if (isNumber(b._board[pos]))
            {
                b._board[pos] = p._mark;
                return true;
            }
            else
            {
                Console.WriteLine("Someone has already played there!");
                return false;
            }
        }

        public void printBoard(Board board)
        {

            //short variable names for each position in the table from a to i, 0-8
            //A B C
            //D E F
            //G H I

            String a, b, c, d, e, f, g, h, i;
            a = getCharacterAtPosition(board, 0);
            b = getCharacterAtPosition(board, 1);
            c = getCharacterAtPosition(board, 2);
            d = getCharacterAtPosition(board, 3);
            e = getCharacterAtPosition(board, 4);
            f = getCharacterAtPosition(board, 5);
            g = getCharacterAtPosition(board, 6);
            h = getCharacterAtPosition(board, 7);
            i = getCharacterAtPosition(board, 8);

            Console.WriteLine("   |   |  ");
            Console.WriteLine(" " + a + " | " + b + " | " + c);
            Console.WriteLine("   |   |  ");
            Console.WriteLine("-----------");
            Console.WriteLine("   |   |  ");
            Console.WriteLine(" " + d + " | " + e + " | " + f);
            Console.WriteLine("   |   |  ");
            Console.WriteLine("-----------");
            Console.WriteLine("   |   |  ");
            Console.WriteLine(" " + g + " | " + h + " | " + i);
            Console.WriteLine("   |   |  ");
        }
        #endregion

        #region PrivateMethods
        private bool checkLine(string a, string b, string c)
        {
            //If the position is a number, then it is a position and has not been marked yet
            //So check if each of the positions are not numbers
            if (!isNumber(a) && !isNumber(b) && !isNumber(c))
            {
                if (a == b && b == c)
                {
                    return true;
                }
            }
            return false;
        }

        private bool isNumber(string a)
        {
            return numbers.Contains(a);
        }

        private string getCharacterAtPosition(Board b, int i)
        {
            return b._board[new Vector2(i)];
            
        }
        #endregion
    }
}
