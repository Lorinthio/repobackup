﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TicTacToe.Model
{
    public class Vector2
    {
        public int X { get; private set; }
        public int Y { get; private set; }

        public Vector2(int pos){
            //Converts a single digit position into an i,j position
            //0 1 2
            //3 4 5
            //6 7 8

            X = pos % 3; //Get left to right orientation
            Y = pos / 3; //Get top to bottom orientation
        }

        public Vector2(int x, int y)
        {
            X = x;
            Y = y;
        }

        public Vector2 setPosition(int pos)
        {
            X = pos % 3; //Get left to right orientation
            Y = pos / 3; //Get top to bottom orientation
            return this;
        }

        public Vector2 setPosition2D(int x, int y)
        {
            X = x;
            Y = y;
            return this;
        }

        public override bool Equals(object other)
        {
            if (other.GetType().IsAssignableFrom(this.GetType()))
            {
                Vector2 vec = (Vector2)other;
                return (X == vec.X && Y == vec.Y);
            }
            return false;
        }

        public override int GetHashCode()
        {
            return X * 3 + Y; //X is large enough to through off chance for an inverse equalw
            /*
            <0,0> == 0
            <0,1> == 1
            <0,2> == 2
            <1,0> == 3
            <1,1> == 4
            etc
            */
        }
    }
}
