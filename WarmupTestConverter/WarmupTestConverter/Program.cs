﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WarmupTestConverter
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("** Warmup Test Case Converter");
            string dirPath = getDirectory();
            string[] files = Directory.GetFiles(dirPath);
            handleWarmups(files);
        }

        static string getDirectory()
        {
            Console.WriteLine("Please paste the directory path of your warmups");
            string path = Console.ReadLine();
            if (Directory.Exists(path))
            {
                return path;
            }
            else
            {
                return getDirectory();
            }
        }

        static void handleWarmups(string[] files)
        {
            foreach(string s in files)
            {
                makeTestCases(s);
                //makeTestMethod(s);
            }
        }

        static void makeTestCases(string file)
        {
            List<string> lines = File.ReadAllLines(file).ToList();
            List<string> cases = new List<string>();
            cases.Add(@"///Test Cases");
            int index = 0;
            bool functionsFound = false;
            foreach(string s in lines)
            {
                if (s.Contains("->") || s.Contains("→"))
                {
                    functionsFound = true;
                    string TestCase = makeTestCase(s);
                    cases.Add(TestCase);
                }
                else
                {
                    if (functionsFound) { index += 1; break; }
                }    
                index += 1;
            }
            cases.Add("");

            for(int i=cases.Count-1; i>= 0; i--)
            {
                lines.Insert(index, cases[i]);
            }

            File.WriteAllLines(file, lines);
            
        }

        static string makeTestCase(string line)
        {
            string prefix = "[TestCase(";
            string center = @"";
            string suffix = ")]";

            bool foundOpen = false;
            bool foundClose = false;
            bool foundArrow = false;

            foreach(char c in line)
            {
                if (!foundOpen)
                {
                    if(c == '(')
                        foundOpen = true;
                }
                else if(!foundClose)
                {
                    if (c == ')')
                    {
                        foundClose = true;
                        center += ",";
                    }
                    else
                    {
                        if (c == '{')
                        {
                            center += "new int[]";
                        }
                        center += c.ToString();
                    }
                        
                }
                else if(!foundArrow)
                {
                    if (c == '>' || c == '→')
                        foundArrow = true;
                }
                else
                {
                    if (c != '\\')
                        center += c.ToString();
                    else
                        break;
                }
            }

            return prefix + center + suffix;
        }
    }
}
