﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Factorizor
{
    class Program
    {
        static void Main(string[] args)
        {
            int number = getInput();
            int total = 0; //used for calculating perfect number
            bool isPrime = true;

            Console.WriteLine("The factors of, " + number + ", are...");
            if (number > 0)
            {
                for(int i=0; i< number; i++)
                {
                    if(((float)number / i) % 1 == 0)
                    {
                        if(i != 1 && i != number)
                        {
                            isPrime = false;
                        }

                        Console.WriteLine(i);
                        total += i;
                    }
                }
            }

            handlePrints(total, number, isPrime);

            Wait();
        }

        public static int getInput()
        {
            bool isValid = false;
            int number;
            do
            {
                Console.Write("What number would you like to factor?");
                isValid = Int32.TryParse(Console.ReadLine(), out number);
                if (!isValid)
                    Console.WriteLine("Invalid input, please input a number...");
            } while (!isValid);

            return number;
        }

        public static void handlePrints(int total, int number, bool isPrime)
        {
            if (total == number)
            {
                Console.WriteLine(number + " is a perfect number");
            }
            else
            {
                Console.WriteLine(number + " is not a perfect number");
            }

            if (isPrime)
            {
                Console.WriteLine(number + " is a prime number");
            }
            else
            {
                Console.WriteLine(number + " is not a prime number");
            }
        }

        static void Wait()
        {
            Console.ReadLine();
        }
    }
}
