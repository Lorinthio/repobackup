﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hangman
{
    class GameManager
    {
        private const string alphabet = "abcdefghijklmnopqrstuvwxyz";
        private enum GameState { Playing, Lost, Won }

        private GameState state = GameState.Playing;
        private Dictionary _dictionary;
        private HangmanStates _hangStates; 
        private string _currentWord;
        private List<char> _chosenLetters;
        private char[] _lettersInWord;
        private int strikes = 0;

        public GameManager()
        {
            SetupGame();
            do
            {
                displayHangman();
                playerTurn();
                checkStates();
            }
            while (state == GameState.Playing);

            Console.Clear();
            displayWord();
            displayResult();
        }

        #region "Setup"
        private void SetupGame()
        {
            _dictionary = new Dictionary();
            _hangStates = new HangmanStates();
            _chosenLetters = new List<char>();
            SetupWord();
        }

        private void SetupWord()
        {
            _currentWord = _dictionary.getRandomWord();
            List<char> wordCharacters = new List<char>();
            foreach(char c in _currentWord)
            {
                if (!wordCharacters.Contains(c))
                {
                    wordCharacters.Add(c);
                }
            }
            _lettersInWord = _currentWord.ToCharArray();
        }
        #endregion

        #region "Display"
        private void displayHangman()
        {
            Console.Clear();
            displayGraphic();
            displayWord();
            displayPlayerLetters();
        }

        private void displayGraphic()
        {
            Console.WriteLine(_hangStates.statesByNumbers[strikes]);
        }

        private void displayWord()
        {
            Console.WriteLine("The word is...\n\n");
            StringBuilder display = new StringBuilder("", 64);
            foreach(char c in this._currentWord)
            {
                if(_chosenLetters.Contains(c)){
                    display.Append(c.ToString() + " ");
                }
                else
                {
                    display.Append("_ ");
                }
            }
            Console.WriteLine(display.ToString() + "\n");
        }

        private void displayPlayerLetters()
        {
            char[] array = _chosenLetters.ToArray();
            Array.Sort(array);
            string letterWord = "Used Letters: ";
            foreach (char c in array)
            {
                letterWord += c.ToString();
            }
            Console.WriteLine(letterWord + "\n");
            _chosenLetters = array.ToList();
        }

        private void displayResult()
        {
            switch (state)
            {
                case GameState.Playing:
                    break;
                case GameState.Lost:
                    Console.WriteLine("You have lost the game...");
                    break;
                case GameState.Won:
                    Console.WriteLine("You have won the game!");
                    break;
            }
        }
        #endregion

        #region "Logic"
        private void playerTurn()
        {
            char letter = handleInput();
            if (_chosenLetters.Contains(letter))
            {
                playerTurn();
            }
            else
            {
                _chosenLetters.Add(letter);
                if (_lettersInWord.Contains(letter))
                {
                    List<char> newList = new List<char>();
                    foreach(char c in _lettersInWord)
                    {
                        if(c != letter)
                        {
                            newList.Add(c);
                        }
                    }
                    _lettersInWord = newList.ToArray();
                }
                else
                {
                    strikes += 1;
                }
            }
        }

        private char handleInput()
        {
            Console.Write("Please enter a letter : ");
            string input = Console.ReadLine();
            if (input.Length >= 1)
            {
                input = input.Substring(0, 1);
                if (alphabet.Contains(input))
                {
                    return input[0];
                }
            }
            return handleInput();
        }

        private void checkStates()
        {
            if (_lettersInWord.Length == 0)
            {
                state = GameState.Won;
            }
            else if (strikes == 7)
            {
                state = GameState.Lost;
            }
            else
            {
                state = GameState.Playing;
            }
            
        }
        #endregion
    }
}
