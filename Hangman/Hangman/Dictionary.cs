﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Hangman
{
    class Dictionary
    {
        private Random random = new Random();
        private string[] words;

        public Dictionary()
        {
            string location = Directory.GetCurrentDirectory() + "\\dictionary.txt";
            words = File.ReadAllLines(location);
        }

        public string getRandomWord()
        {
            return words[random.Next(0, words.Length)].Trim();
        }
    }
}
