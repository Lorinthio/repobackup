﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hangman
{
    class HangmanStates
    {

        #region "StoredImages"
        public string zero = @"
  ___
 |   
 |   
 |
 |  
---
";
        public string one = @"
  ___
 |   |
 |  
 |
 |  
---
";
        public string two = @"
  ___
 |   |
 |   O
 |
 |  
---
";
        public string three = @"
  ___
 |   |
 |   O
 |  /
 |  
---
";
        public string four = @"
  ___
 |   |
 |   O
 |  /|
 |  
---
";
        public string five = @"
  ___
 |   |
 |   O
 |  /|\ 
 |  
---
";
        public string six = @"
  ___
 |   |
 |   O
 |  /|\ 
 |  /
---
";
        public string seven = @"
  ___
 |   |
 |   O
 |  /|\ 
 |  / \
---
";
        #endregion

        public Dictionary<int, string> statesByNumbers;
        public HangmanStates()
        {
            statesByNumbers = new Dictionary<int, string>();
            statesByNumbers.Add(0, zero);
            statesByNumbers.Add(1, one);
            statesByNumbers.Add(2, two);
            statesByNumbers.Add(3, three);
            statesByNumbers.Add(4, four);
            statesByNumbers.Add(5, five);
            statesByNumbers.Add(6, six);
            statesByNumbers.Add(7, seven);
        }
    }
}
