﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Battleship
{
    public enum ShipType { Destroyer, Submarine, Cruiser, Carrier, Battleship }
    public enum ShotType { Invalid, Duplicate, Empty, Miss, Hit, Sunk, Victory}
    public enum Direction { Unknown, North, South, East, West}
    

    public class Helper {
        static string letters = "abcdefghij";
        public static int GetLengthOfShip(ShipType type)
        {
            switch (type)
            {
                case ShipType.Destroyer:
                    return 2;
                case ShipType.Submarine:
                    return 3;
                case ShipType.Cruiser:
                    return 3;
                case ShipType.Carrier:
                    return 4;
                case ShipType.Battleship:
                    return 5;
            }
            return 0;
        }
        public static int GetNumberFromLetter(string letter)
        {
            return (letters.IndexOf(letter.Substring(0,1).ToLower()));
        }
        public static string GetLetterFromNumber(int number)
        {
            return (letters[number].ToString().ToUpper());
        }
        public static bool IsAlpha(string input)
        {
            return letters.Contains(input);
        }
        public static Direction GetDirectionFromLetter(string letter)
        {
            letter = letter.Substring(0, 1).ToLower();

            if(letter == "n")
            {
                return Direction.North;
            }
            else if(letter == "s")
            {
                return Direction.South;
            }
            else if(letter == "e")
            {
                return Direction.East;
            }
            else if(letter == "w")
            {
                return Direction.West;
            }
            else
            {
                return Direction.Unknown;
            }
        }
    }
    
}
