﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Battleship.Connection
{
    public class Server
    {
        TcpListener server;
        TcpClient client;
        public bool isConnected = false;
        public StreamWriter writer;
        public StreamReader reader;

        public Server()
        {
            server = new TcpListener(7777);
            server.Start();

            try
            {
                Console.Clear();
                Console.WriteLine("Waiting for connection...");
                client = server.AcceptTcpClient();
                writer = new StreamWriter(client.GetStream());
                reader = new StreamReader(client.GetStream());
                isConnected = client.Connected;
            }
            catch (Exception e)
            {

            }
        }
    }
}
