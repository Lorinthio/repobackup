﻿using Battleship.Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Battleship.Connection
{
    class ConnectedGame
    {
        string yourTurn = @"
====================================
            YOUR TURN
====================================
Select a coordinate to shoot at! (e.g. 'A,2')
";

        //Our Players Info
        Board mine;
        Board theirs;

        //Network Data
        StreamWriter writer;
        StreamReader reader;
        Thread dataThread;
        string move = "";
        bool myTurn = true;
        bool ready = false; //Opponent is ready to go
        bool finished = false; //Game is complete
        int opponentShipsPlaced = 1;

        static List<string> statusUpdates = new List<string>();

        int shipsKilled; //Their ships sunk
        int shipsDown; //Our ships have sunk

        public ConnectedGame(StreamWriter writer, StreamReader reader, bool isServer)
        {
            Console.Clear();

            myTurn = isServer; //Server will go first
            this.writer = writer;
            writer.AutoFlush = true;
            this.reader = reader;
            dataThread = new Thread(this.getData);
            dataThread.Start();

            mine = new Board();
            theirs = new Board();
            PlaceShips();

            PlayGame();
        }

        public static void AddStatusMessage(string line)
        {
            statusUpdates.Add(line);
        }

        #region "Setup"
        public void PlaceShips()
        {
            foreach (ShipType type in Enum.GetValues(typeof(ShipType)))
            {
                bool valid = false;
                while (!valid)
                {
                    valid = PlaceShip(type);
                    if (valid)
                    {
                        writer.WriteLine(move);
                    }
                    Console.Clear();
                }
            }
            writer.WriteLine("SETUPCOMPLETE");
            idle(); //Until opponent is done
        }

        private void idle()
        {
            
            while (!ready)
            {
                Console.Clear();
                Console.WriteLine("Opponent is placing ship #{0}", opponentShipsPlaced);
                Thread.Sleep(100);
            }
        }

        #region placement
        private bool PlaceShip(ShipType type)
        {
            mine.DrawBoard(true);
            Console.WriteLine("Place your ships by entering the coordinates and direction \nNorth(n), South(s), East(e), or West(w)\nSeparate each item with a ','\nExample: A,10,n");
            string name = type.ToString();
            int length = Helper.GetLengthOfShip(type);
            Console.WriteLine("Current Ship: " + name + "\n" + "Ship Length: " + length);
            string userInput = Console.ReadLine();

            string[] input = userInput.Split(',');
            if(input.Length < 3)
            {
                return false;
            }
            foreach(string s in input)
            {
                if(s == "")
                {
                    return false;
                }
            }
            if (!Helper.IsAlpha(input[0]))
                return false;
            int x = Helper.GetNumberFromLetter(input[0]);
            int y;
            if (!int.TryParse(input[1], out y))
                return false;
            y -= 1;
            Direction d = Helper.GetDirectionFromLetter(input[2]);
            if (d == Direction.Unknown)
                return false;

            if (x < 0 || x > 9 || y < 0 || y > 9)
                return false;

            List<string> args = input.ToList();
            args[0] = x.ToString();
            args.Add(type.ToString());

            if (!Place(args.ToArray(), true))
            {
                return false;
            }

            userInput = "";
            foreach(string s in args)
            {
                userInput += s + ",";
            }
            move = "{PLACE}:" + userInput + type.ToString();
            return true;
        }

        //Should never return false when sent over network as it is checked
        //before it is sent
        private bool Place(string[] args, bool mine)
        {
            int x, y, length;
            Direction d;
            
            Board board;

            if (mine)
                board = this.mine;
            else
                board = theirs;

            x = int.Parse(args[0]);
            y = int.Parse(args[1]) - 1;
            d = Helper.GetDirectionFromLetter(args[2]);
            ShipType type = (ShipType) Enum.Parse(typeof(ShipType), args[3]);
            ShipCoord ship = new ShipCoord(board, type);
            length = Helper.GetLengthOfShip(type) - 1;

            switch (d)
            {
                case Direction.North:
                    if ((y - length) < 0)
                        return false;
                    for (int i = 0; i <= length; i++)
                    {
                        Coord c = new Coord(x, y - i);
                        if (!board.isEmpty(c))
                            return false;
                    }
                    for (int i = 0; i <= length; i++)
                    {
                        Coord c = new Coord(x, y - i);
                        board.Place(ship, c);
                    }
                    break;
                case Direction.South:
                    if ((y + length >= 10))
                        return false;
                    for(int i=0; i<=length; i++)
                    {
                        Coord c = new Coord(x, y + i);
                        if (!board.isEmpty(c))
                            return false;
                    }
                    for (int i=0; i<= length; i++)
                    {
                        Coord c = new Coord(x, y + i);
                        board.Place(ship, c);
                    }
                    break;
                case Direction.East:
                    if ((x + length >= 10))
                        return false;
                    for (int i = 0; i <= length; i++)
                    {
                        Coord c = new Coord(x + i, y);
                        if (!board.isEmpty(c))
                            return false;
                    }
                    for (int i = 0; i <= length; i++)
                    {
                        Coord c = new Coord(x + i, y);
                        board.Place(ship, c);
                    }
                    break;
                case Direction.West:
                    if ((x - length < 0))
                        return false;
                    for (int i = 0; i <= length; i++)
                    {
                        Coord c = new Coord(x - i, y);
                        if (!board.isEmpty(c))
                            return false;
                    }
                    for (int i = 0; i <= length; i++)
                    {
                        Coord c = new Coord(x - i, y);
                        board.Place(ship, c);
                    }
                    break;

            }
            return true;
        }
        #endregion
        #endregion

        #region "connectionData"
        private void getData()
        {
            while (!this.finished)
            {
                try
                {
                    writer.WriteLine("{PING}:{PONG}");
                    Thread.Sleep(100);
                    if (!reader.EndOfStream)
                    {
                        string line = reader.ReadLine();
                        //Console.WriteLine(line);
                        handleData(line);
                    }
                }
                catch (IOException e)
                {
                    //Console.WriteLine("IOERROR");
                }
            }
        }

        private void handleData(string line)
        {
            if(line.Contains("SETUPCOMPLETE"))
            {
                ready = true;
            }
            else
            {
                string[] args;
                string[] commandsplit = line.Split(':');
                string type = commandsplit[0];
                switch (type)
                {
                    case "{PLACE}":
                        args = commandsplit[1].Split(',');
                        opponentShipsPlaced = opponentShipsPlaced + 1;
                        Place(args, false);
                        break;
                    case "{PING}":
                        //writer.WriteLine("{PING}:{PONG}");
                        break;
                    case "{FIRE}":
                        args = commandsplit[1].Split(',');
                        RecieveFire(args);
                        myTurn = true;
                        break;
                }
            }
        }

        private void RecieveFire(string[] args)
        {
            int x = int.Parse(args[0]);
            int y = int.Parse(args[1]);

            mine.Fire(x, y, true);
        }

        #endregion

        private void PlayGame()
        {
            Console.WriteLine("PLAY GAME");
            Console.Clear();
            //Enemies board is the top
            theirs.DrawBoard(false);
            Console.WriteLine("======================");
            //Your board is on the bottom
            mine.DrawBoard(true);
            if(mine.shipsAlive == 0)
            {
                Console.Clear();
                myTurn = false;
                finished = true;
                Console.WriteLine(@"
====================================
        ...You have lost...
====================================
");
                
            }
            else if(theirs.shipsAlive == 0)
            {
                Console.Clear();
                myTurn = false;
                finished = true;
                Console.SetWindowSize(55, 60);
                Console.WriteLine(@"
====================================
           You have Won!
====================================
                                     |__
                                     |\/
                                     ---
                                     / | [
                              !      | |||
                            _/|     _/|-++'
                        +  +--|    |--|--|_ |-
                     { /|__|  |/\__|  |--- |||__/
                    +---------------___[}-_===_.'____                 /\
                ____`-' ||___-{]_| _[}-  |     |_[___\==--            \/   _
 __..._____--==/___]_|__|_____________________________[___\==--____,------' .7
|                                                                     BB-61/
 \_________________________________________________________________________|
");
            }

            if (myTurn)
            {
                takeTurn();
            }
            else
            {
                while (!myTurn)
                {
                    Thread.Sleep(100);
                }
            }

            PlayGame();
        }

        public void takeTurn()
        {
            
            if(statusUpdates.Count != 0)
            {
                Console.WriteLine(@"
====================================
           Status Updates
====================================
");
                foreach (string s in statusUpdates)
                {
                    string line = ChangeFontColor(s);
                    Console.WriteLine(line);
                    Console.ForegroundColor = ConsoleColor.White;
                }
            }
            
            statusUpdates = new List<string>();
            Console.WriteLine(yourTurn);
            string[] pos = Console.ReadLine().Split(',') ;
            int x;
            int y = 0;
            if(!Helper.IsAlpha(pos[0]))
            {
                PlayGame();
            }
            if (!int.TryParse(pos[1], out y))
            {
                PlayGame();
            }
            x = Helper.GetNumberFromLetter(pos[0]);
            y -= 1;

            if (theirs.Fire(x, y, false))
            {
                myTurn = false;
                writer.WriteLine("{FIRE}:" + x.ToString() + "," + y.ToString());
            }
            else
            {
                PlayGame();
            }
        }

        public string ChangeFontColor(string s)
        {
            if (s.Contains("[CCRED]"))
            {
                Console.ForegroundColor = ConsoleColor.Red;
                s = s.Substring(7, s.Length-7);
            }
            else if (s.Contains("[CCYELLOW]"))
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
                s = s.Substring(9, s.Length - 9);
            }

            return s;
        }
    }
}
