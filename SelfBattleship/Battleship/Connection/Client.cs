﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.IO;
using System.Threading;

namespace Battleship.Connection
{
    public class Client
    {
        TcpClient client;
        public bool isConnected = false;
        public StreamWriter write;
        public StreamReader read;

        public Client(string ip)
        {
            try
            {
                client = new TcpClient();
                client.Connect(ip, 7777);
                isConnected = client.Connected;
                write = new StreamWriter(client.GetStream());
                read = new StreamReader(client.GetStream());
                
            }
            catch(SocketException e)
            {
                //Break silently
            }
        }
    }
}
