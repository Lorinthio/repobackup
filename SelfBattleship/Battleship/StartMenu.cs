﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Battleship.Connection;

namespace Battleship
{
    class StartMenu
    {
        string splashScreen = @"
    ___  __  ____  ____       ____ ____  __  __  ______  ____
   /__/ /__|  //    //  /    /___ /___  / /_/ /    /    /___/
  /__/ /   | //    //  /___ /___  ___/ /_/ /_/  __/__  /
";
        string menu = @"
C) Connect
H) Host
Q) Quit
";

        public void StartScreen()
        {
            Console.SetWindowSize(80,55);

            Console.Clear();
            Console.Write(splashScreen);
            Console.Write(menu);

            Console.WriteLine("\n\n\nPress any key to begin!");
            getInput();
            
        }

        void getInput()
        {
            ConsoleKeyInfo key = Console.ReadKey();
            if (key.Key == ConsoleKey.C)
            {
                startClient();
            }
            else if (key.Key == ConsoleKey.H)
            {
                startServer();
            }
            else
            {
                getInput();
            }
        }

        public void startClient()
        {
            Console.Clear();
            Console.Write("Host IP: ");
            string ip = Console.ReadLine();
            Client c = new Client(ip);
            if (!c.isConnected)
            {
                Console.WriteLine("Connection failed, Try again? (y/n) ");
                string answer = Console.ReadLine();
                if(answer.Substring(0,1) == "y")
                {
                    startClient();
                }
                else
                {
                    StartScreen();
                }
            }
            else
            {
                new ConnectedGame(c.write, c.read, false);
            }
        }

        public void startServer()
        {
            Server server = new Server();
            if (server.isConnected)
            {
                new ConnectedGame(server.writer, server.reader, true);
            }
        }
    }

    
}
