﻿using Battleship.Connection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Battleship.Data
{
    public class ShipCoord
    {
        public ShipType type;
        int health;
        Board board;

        public ShipCoord(Board board, ShipType type)
        {
            this.board = board;
            this.type = type;
            health = Helper.GetLengthOfShip(type);
        }

        public bool Hit(bool isMine)
        {
            health -= 1;
            if(health == 0)
            {
                if (isMine)
                {
                    ConnectedGame.AddStatusMessage("[CCRED]They sunk your " + type.ToString() + "!!!");
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.WriteLine("You sunk their " + type.ToString() + "!!!");
                }
                board.setShipSunk(this);
                return true;
            }
            return false;
        }
    }
}
