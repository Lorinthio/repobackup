﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Battleship.Data
{
    public class Coord
    {
        public int X;
        public int Y;

        public Coord(int x, int y)
        {
            X = x;
            Y = y;
        }

        public override int GetHashCode()
        {
            return (X * 10 + Y);
        }

        public override string ToString()
        {
            return "<" + X + ", " + Y + ">";
        }

        public override bool Equals(Object obj)
        {
            if(obj is Coord) {
                Coord other = (Coord)obj;
                return (other.X == X && other.Y == Y);
            }
            return false;
            
        }
    }
}
