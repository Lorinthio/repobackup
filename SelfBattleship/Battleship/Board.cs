﻿using Battleship.Connection;
using Battleship.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Battleship
{
    public class Board
    {
        Dictionary<Coord, ShotType> ShotHistory;
        Dictionary<Coord, ShipCoord> ShipPosition;

        public int shipsAlive = 5;
        
        public Board()
        {
            ShotHistory = new Dictionary<Coord, ShotType>();
            ShipPosition = new Dictionary<Coord, ShipCoord>();
            for(int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 10; j++)
                {
                    ShotHistory.Add(new Coord(i,j), ShotType.Empty);
                }
            }
            /*foreach(KeyValuePair<Coord, ShotType> pair in ShotHistory)
            {
                Console.WriteLine("key = " + pair.Key + "Value = " + pair.Value);
            }
            */
        }

        public void DrawBoard(bool drawShips)
        {
            Console.WriteLine("  A B C D E F G H I J\n");
            Coord pos;
            for (int i = 0; i < 10; i++)
            {
                Console.Write(i + 1);
                if (i < 9)
                    Console.Write(" ");
                for(int j = 0; j < 10; j++)
                {
                    pos = new Coord(j, i);
                    ShotType currentShot = ShotHistory[pos];
                    PrintShot(pos, currentShot, drawShips);
                    Console.ResetColor();
                    Console.Write(" ");
                }
                Console.ResetColor();
                Console.WriteLine("\n");
            }
        }

        public bool Fire(int x, int y, bool isMine)
        {
            if (isMine)
            {
                ConnectedGame.AddStatusMessage(string.Format("They shot at ({0}, {1})...", Helper.GetLetterFromNumber(x),y));
            }

            Coord pos = new Coord(x, y);
            if (ShotHistory[pos] != ShotType.Empty)
            {
                return false;
            }

            ShotType type = ShotType.Miss;
            if (this.ShipPosition.ContainsKey(pos))
            {
                type = ShotType.Hit;
                if(isMine)
                    ConnectedGame.AddStatusMessage(string.Format("And hit your {0}!", ShipPosition[pos].type));
                if (ShipPosition[pos].Hit(isMine))
                {
                    shipsAlive -= 1;
                    return true;
                }
            }
            else
            {
                if(isMine)
                    ConnectedGame.AddStatusMessage(string.Format("And missed!"));
            }
            ShotHistory[pos] = type;
            return true;
        }

        public void PrintShot(Coord c, ShotType shot, bool drawShips)
        {
            switch (shot)
            {
                case ShotType.Hit:
                    Console.BackgroundColor = ConsoleColor.White;
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.Write("H");
                    break;
                case ShotType.Empty :
                    if (drawShips)
                    {
                        if (ShipPosition.ContainsKey(c)) {
                            Console.BackgroundColor = ConsoleColor.White;
                        }
                        else
                        {
                            Console.BackgroundColor = ConsoleColor.Blue;
                        }
                    }
                    else
                    {
                        Console.BackgroundColor = ConsoleColor.Blue;
                    }
                    
                    Console.Write(" ");
                    break;
                case ShotType.Miss:
                    Console.BackgroundColor = ConsoleColor.Blue;
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.Write("M");
                    break;
                case ShotType.Sunk:
                    Console.BackgroundColor = ConsoleColor.Red;
                    Console.Write(" ");
                    break;
            }
        }
        public void setShipSunk(ShipCoord c)
        {
            foreach(KeyValuePair<Coord, ShipCoord> pair in this.ShipPosition)
            {
                if (pair.Value.Equals(c))
                {
                    ShotHistory[pair.Key] = ShotType.Sunk;
                }
            }
        }
        public bool isEmpty(Coord c)
        {
            return !this.ShipPosition.ContainsKey(c);
        }

        public void Place(ShipCoord c, Coord c2)
        {
            this.ShipPosition[c2] = c;
        }
    }
}
